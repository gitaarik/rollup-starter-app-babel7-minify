(function () {
  'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  var App2 =
  /*#__PURE__*/
  function () {
    function App2() {
      _classCallCheck(this, App2);
    }

    _createClass(App2, [{
      key: "test",
      value: function test() {
        document.getElementById('app2').innerHTML = 'it works!';
      }
    }]);

    return App2;
  }();

  var app2 = new App2();
  app2.test();

}());
//# sourceMappingURL=app2.es5.js.map
