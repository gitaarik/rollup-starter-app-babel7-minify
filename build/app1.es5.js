(function () {
  'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  var App1 =
  /*#__PURE__*/
  function () {
    function App1() {
      _classCallCheck(this, App1);
    }

    _createClass(App1, [{
      key: "test",
      value: function test() {
        document.getElementById('app1').innerHTML = 'it works!';
      }
    }]);

    return App1;
  }();

  var app1 = new App1();
  app1.test();

}());
//# sourceMappingURL=app1.es5.js.map
