# rollup-starter-app-babel7-minify

This is my version of rollup-starter-app that also uses Babel 7 to transpile
and babel-minify to minify the code. It creates 4 output files for each input
file:

- ES5 non-minified (my-app.es5.js)
- ES5 minified (my-app.es5.min.js)
- ES6 non-minified (my-app.js)
- ES6 minified (my-app.min.js)

It also creates sourcemaps for all these files. You can serve the ES6 file to
modern browsers and ES5 files for older browsers like this:

    <script type="module" src="my-app.min.js"></script>
    <script nomodule src="my-app.es5.min.js"></script>

Old browsers don't support ES modules so will ignore the `<script
type="module">` tags, and modern browsers will ignore `<script nomodule>`. IE11
is supported by this but not IE10.

Steps to start using this:

    git clone git@gitlab.com:gitaarik/rollup-starter-app-babel7-minify.git
    cd rollup-starter-app-babel7-minify
    yarn install
    npm run build

Then you can view the examples (app1.html and app2.html) in the build/ folder.
